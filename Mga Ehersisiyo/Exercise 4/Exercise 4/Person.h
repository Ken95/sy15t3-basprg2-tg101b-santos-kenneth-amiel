#pragma once
#ifndef PERSON_H
#define PERSON_H

#include <string>
using namespace std;

class Person {
public:
	void attack();
	void defend();
	void equip();
	void skill();
	void levelUp();
	void die();

private:
	string mName;
	string mWeapon;
	string mAccesories;
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;
	int mAttack;
	int mDefense;
	int mAttackRatio;
	int mDefenseRatio;
	int mMagicAttack;
	int mMagicDefend;
	int mMagicDefendRatio;
};

#endif //PERSON_H