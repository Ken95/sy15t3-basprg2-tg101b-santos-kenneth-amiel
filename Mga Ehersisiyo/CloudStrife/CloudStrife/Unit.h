#pragma once

#include <string>
using namespace std;

class Unit
{
public:
	Unit(string name);
	~Unit();

	// Basic info
	string getName();
	int getLevel();
	int getHp();
	int getMaxHp();
	int getMp();
	int getMaxMp();
	int getCurrentXp();

	// Primary Stats
	int getStrength();
	int getDexterity();
	int getVitality();
	int getMagic();
	int getSpirit();
	int getLuck();

	// Derived Stats
	int getAttack();
	float getAttackPercent(); 
	int getDefense();
	float getDefensePercent();
	int getMagicAtk();
	int getMagicDef();
	float getMagicDefPercent();

private:
	// Basic info
	string mName;
	int mLevel;
	int mHp;
	int mMaxHp;
	int mMp;
	int mMaxMp;
	int mCurrentXp;

	// Primary Stats
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;

	// Derived Stats
	int mAttack;
	float mAttackPercent;
	int mDefense;
	float mDefensePercent;
	int mMagicAtk;
	int mMagicDef;
	float mMagicDefPercent;
};

