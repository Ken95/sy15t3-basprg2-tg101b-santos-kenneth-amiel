#include <iostream>
#include <string>

using namespace std;

int getInput(); // gets the input from the user
void factorial(int input); // gets the factorial of the number inputted

int main()
{
	int input = getInput();

	factorial(input);

	system("pause");
	return 0;
}



int getInput() {

	int input;

	cout << "Enter a number: ";
	cin >> input;

	return input;
}

void factorial(int input) {

	int product = 1;
	for (int x = 1; x <= input; x++)
	{
		product *= x;
	}

	cout << "The factorial of " << input << " is " << product << endl;
}