#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

void fillArray(int* elements);

int main()
{
	srand(time(NULL));
	int* elementsu = new int[7];

	fillArray(elementsu);	

	for (int i = 0; i < 7; i++)
	{
		cout << *(elementsu + i) << endl;
	}

	delete[] elementsu; elementsu = NULL;
	system("pause");
	return 0;
}

void fillArray(int* elements)
{
	for (int i = 0; i < 7; i++)
	{
		*elements = rand() % 100;
		elements++;
	}
}