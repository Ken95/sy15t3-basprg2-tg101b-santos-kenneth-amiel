#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

struct Roll
{
	int dice[3];
};

Roll* rollDice()
{
	Roll* dice = new Roll;

	for (int i = 0; i < 3; i++)
	{
		dice->dice[i] = rand() % 6 + 1;
	}

	return dice;
}

void playRound()
{
	for (int i = 0; i < 3; i++) {
		
		Roll* dice = rollDice();
		cout << dice->dice[i] << endl;
		delete dice; dice = NULL;
	}


	
}

int main()
{
	srand(time(NULL));

	for (int i = 1;i <= 10; i++)
	{
		cout << "Round " << i << endl;
		playRound();
		system("pause>nul");
		system("cls");
	}

		
	

	return 0;
}