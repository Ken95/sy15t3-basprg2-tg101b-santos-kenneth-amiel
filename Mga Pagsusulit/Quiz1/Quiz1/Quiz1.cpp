//=========================================================================================
//===============         QUIZ NO. 1: FREEMIUM PACKAGES SUGGESTIONS         ===============
//=========================================================================================
#include <iostream>
#include <string>

using namespace std;

// Ask the user the price of the item they want to buy
int getInput(int currentMoney);

// Buying Process
int shopping(int itemPrice, int currentMoney);

// Sorting the Packages
int packages(int itemPrice, int currentMoney);

// Ask the user if they want to shop again
char choice(char continueShopping);


int main()
{
	int currentMoney = 250;
	int itemPrice;
	char continueShopping;


	cout << "Do you want to go shopping? (Y)es / (N)o" << endl;
	cin >> continueShopping;
	while (continueShopping == 'y' || continueShopping == 'Y')
	{
		itemPrice = getInput(currentMoney);
		currentMoney = shopping(itemPrice, currentMoney);
		currentMoney = packages(itemPrice, currentMoney);
		continueShopping = choice(continueShopping);

		if (continueShopping == 'n')
		{
			cout << "Please Come Again! " << endl;
			break;
		}
		else if (continueShopping == 'x')
		{
			cout << "INVALID ANSWER! THAT MEANS YES!" << endl;
		}

	}



	system("pause");
	return 0;
}




int getInput(int currentMoney)
{
	int itemPrice;
	cout << "Remaining Credits: " << currentMoney << endl;
	cout << "Price of the item to buy: ";
	cin >> itemPrice;

	if (itemPrice > currentMoney) return itemPrice;
	else if (itemPrice <= 0) return itemPrice;

	return itemPrice;
}



int shopping(int itemPrice, int currentMoney)
{
	if (itemPrice > 0 && itemPrice < currentMoney)
	{
		char choice;
		cout << "Are you Sure? (Y)es / (N)o " << endl;
		cin >> choice;

		if (choice == 'y' || choice == 'Y') {
			cout << "Item bought successfully!" << endl;
			currentMoney -= itemPrice;
			return currentMoney;
		}

		else if (choice == 'n' || choice == 'N')
		{
			cout << "Transaction Cancelled" << endl;
			return currentMoney;
		}

	}

	else if (itemPrice == 0)
	{
		cout << "You cannot buy that kind of item! Please Try Again!" << endl;
		return currentMoney;
	}

	else if (itemPrice > currentMoney)
	{
		cout << "You don't have enough credits to buy this item" << endl;

	}

	return currentMoney;
}



int packages(int itemPrice, int currentMoney)
{
	int packages[7] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int j, temp;

	for (int i = 0; i < 7; i++) {
		j = i;
		while (j > 0 && packages[j] < packages[j - 1]) {
			temp = packages[j];
			packages[j] = packages[j - 1];
			packages[j - 1] = temp;
			j--;
		}
	}

	char choice;
	int i = 0;
	bool search = false;
	while (i < 7)
	{
		if ((currentMoney + packages[i]) > itemPrice)
		{
			search = true;
			break;
		}

		i++;

	}

	if (search)
	{
		cout << "Do you want to avail Package " << i + 1 << "(" << packages[i] << ")? (Y)es / (N)o " << endl;
		cin >> choice;
		if (choice == 'y' || choice == 'Y')
		{
			currentMoney += packages[i];
			return currentMoney;
		}
		else if (choice == 'n' || choice == 'N')
		{
			return currentMoney;
		}
	}
	else
	{
		cout << "Sorry, you cannot avail any package with that price. Buy another item." << endl;
		return currentMoney;
	}
	return currentMoney;
}


char choice(char continueShopping)
{

	cout << "Shop Again? (Y)es / (N)o " << endl;
	cin >> continueShopping;

	if (continueShopping == 'y' || continueShopping == 'Y') return 'y';
	else if (continueShopping == 'n' || continueShopping == 'N') return 'n';

	return 'x';
}

