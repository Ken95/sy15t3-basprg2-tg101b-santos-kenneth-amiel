#include "Spawner.h"



Spawner::Spawner()
{
	mUnit = NULL;
	mNumberOfSpawns = 0;
}

Spawner::~Spawner()
{
	delete mUnit;
	mUnit = NULL;
}

Unit* Spawner::spawn()
{
	System::Random ^r = gcnew System::Random();
	mRandomClass = r->Next(1,4);

	mUnit = new Unit(mRandomClass, mNumberOfSpawns);
	mNumberOfSpawns++;

	return mUnit;
}

int Spawner::getNumberOfSpawns()
{
	return mNumberOfSpawns;
}
