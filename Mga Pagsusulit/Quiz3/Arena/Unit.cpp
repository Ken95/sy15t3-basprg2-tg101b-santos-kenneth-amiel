#include "Unit.h"


Unit::Unit(string name, int classChoice)
{
	mName = name;
	
	setClassStats(classChoice);
}

Unit::Unit(int classChoice, int level)
{
	mName = "Enemy";

	setClassStats(classChoice);
	 
	mMaxHp = 50 + level*3.5;
	mHp = mMaxHp;
	mPower = 30 + level*3.5;
	mVitality = 15 + level*3.5;
	mAgility = 30 + level*3.5;
	mDexterity = 15 + level*3.5;
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mClass;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getAgility()
{
	return mAgility;
}

int Unit::getDexterity()
{
	return mDexterity;
}

int Unit::getDamage()
{
	return mDamage;
}

int Unit::getHitRate()
{
	return mHitrate;
}

int Unit::getHeal()
{
	return mHeal;
}

int Unit::getBonusDamage()
{
	return mBonusDamage;
}

bool Unit::getFirstToAttack()
{
	return mFirstToAttack;
}

bool Unit::getHit()
{
	return mHit;
}

string Unit::getTargetClass()
{
	return mTargetClass;
}

void Unit::setClassStats(const int classChoice)
{
	if (classChoice == CHOICE_WARRIOR) {
		mClass		= CLASS_WARRIOR;
		mMaxHp		= 100;
		mHp			= mMaxHp;
		mPower		= 50;
		mVitality	= 30;
		mAgility	= 50;
		mDexterity	= 20;

		return;
	}

	else if (classChoice == CHOICE_ASSASSIN) {
		mClass		= CLASS_ASSASSIN;
		mMaxHp		= 80;
		mHp			= mMaxHp;
		mPower		= 40;
		mVitality	= 10;
		mAgility	= 60;
		mDexterity	= 30;

		return;
	}

	else if (classChoice == CHOICE_MAGE) {
		mClass		= CLASS_MAGE;
		mMaxHp		= 90;
		mHp			= mMaxHp;
		mPower		= 60;
		mVitality	= 20;
		mAgility	= 40;
		mDexterity	= 10;

		return;
	}

	throw exception("Error! Class not part of this game. ");
}

void Unit::attack(Unit * target)
{
	// Determine Whether the attack will Hit/Miss Based on the hit rate
	System::Random ^r = gcnew System::Random();
	mChanceOfHit = r->Next(1, 101);

	if (mChanceOfHit > mHitRate) {
		mHit = ATTACK_MISS;
		return;
	}


	// Deducts the targets Hp by the Damage given by this Unit (if the attack didn't miss)
	target->mHp -= mDamage;
	mHit = ATTACK_HIT;

	// if target HP is negative, set it to zero
	if (target->mHp < 0) target->mHp = 0;
}

void Unit::check(const Unit * target)
{
	// The Unit Keeps in Mind his target's Class
	if (target->mClass == CLASS_WARRIOR)		mTargetClass = CLASS_WARRIOR;
	else if (target->mClass == CLASS_ASSASSIN)	mTargetClass = CLASS_ASSASSIN;
	else if (target->mClass == CLASS_MAGE)		mTargetClass = CLASS_MAGE;


	// Checks if the Unit is Advantageous Against his Target
	if (mClass == CLASS_WARRIOR) {
		if (target->mClass == CLASS_ASSASSIN) mAdvantage = ADVANTAGE;
		else mAdvantage = false;
	}

	else if (mClass == CLASS_ASSASSIN) {
		if (target->mClass == CLASS_MAGE) mAdvantage = ADVANTAGE;
		else mAdvantage = false;
	}

	else if (mClass == CLASS_MAGE) {
		if (target->mClass == CLASS_WARRIOR) mAdvantage = ADVANTAGE;
		else mAdvantage = false;
	}

	// Determine if this Unit Will Attack first
	if (mAgility >= target->mAgility) mFirstToAttack = true;
	else mFirstToAttack = false;

	// Determines Damage, Bonus Damage ( If Applicable ), and Hit Rate Based on targets stats
	mDamage = mPower - target->mVitality;
	if (mDamage < DAMAGE_MIN) mDamage = DAMAGE_MIN;

	mBonusDamage = mDamage * MULTIPLIER_DAMAGE;

	// Evaluates Damage Based on advantage
	if (mAdvantage) mDamage += mBonusDamage;

	mHitRate = ((float)mDexterity / (float)target->mAgility) * 100;

	//  Clamps the Hit Rate within the Hit Rate Range
	if (mHitRate > HIT_RATE_MAX) mHitRate = HIT_RATE_MAX;
	else if (mHitRate < HIT_RATE_MIN) mHitRate = HIT_RATE_MIN;
}

void Unit::lvlUp()
{
	// Determine Win Bonus Based on the target that he kept in mind
	if (mTargetClass == CLASS_WARRIOR) {
		mMaxHp += BONUS_THREE;
		mVitality += BONUS_THREE;
	}

	else if (mTargetClass == CLASS_ASSASSIN) {
		mAgility += BONUS_THREE;
		mDexterity += BONUS_THREE;
	}

	else if (mTargetClass == CLASS_MAGE) {
		mPower += BONUS_FIVE;
	}

	// Heals the player 30% of his Maximum HP
	mHeal = mMaxHp * MULTIPLER_HEAL;
	mHp += mHeal;

	// If HP is over the Maximum Limit, Set it to FULL
	if (mHp > mMaxHp) mHp = mMaxHp;
}
	