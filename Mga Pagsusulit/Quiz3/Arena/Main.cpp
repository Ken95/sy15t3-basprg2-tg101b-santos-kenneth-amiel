#include "Unit.h"
#include "Spawner.h"

// Create player
Unit* createPlayer();

// Display Stats
void displayStats(Unit* unit);

// Play 1 round
void playRound(Unit*player, Unit* enemy);

// Simulate 1 attack (implement Higher agility gets to atttack first)
void combat(Unit* player, Unit* enemy);


int main()
{
	Unit* player = createPlayer();
	Unit* enemy = NULL;

	Spawner *spawner = new Spawner();

	int round = 1;

	while (true) {
		cout << "Round " << round << endl;
		enemy = spawner->spawn();

		player->check(enemy);
		enemy->check(player);

		playRound(player, enemy);

		// if player is dead, end the game
		if (player->getHp() == 0) break;

		// else, add stats
		player->lvlUp();
		if (player->getTargetClass() == CLASS_WARRIOR)			cout << "HP +"  << BONUS_THREE << " VIT +" << BONUS_THREE << endl;
		else if (player->getTargetClass() == CLASS_ASSASSIN)	cout << "AGI +" << BONUS_THREE << " DEX +" << BONUS_THREE << endl;
		else if (player->getTargetClass() == CLASS_MAGE)		cout << "POW +" << BONUS_FIVE							  << endl;

		cout << "You were healed " << player->getHeal() << " HP" << endl;

		// Deallocate the Enemy Defeated
		delete enemy;
		enemy = NULL;

		cout << "You defeated an enemy" << endl;
		system("pause>nul");
		system("cls");

		round++;
	}

	cout << "You were defeated! " << endl;
	system("pause>nul");
	system("cls");

	cout << "You Defeated " << spawner->getNumberOfSpawns() << " enemies" << endl;
	system("pause>nul");
	system("cls");
	
	cout << "Congratulations!  Here are your Final Stats: " << endl << endl;
	displayStats(player);
	system("pause>nul");
	system("cls");

	delete player;
	player = NULL;
	delete spawner;
	spawner = NULL;

	return 0;
}

Unit* createPlayer()
{
	Unit* unit = NULL;
	string name;
	int classChoice;

	cout << "Enter your Name: ";
	getline(cin, name);
	system("cls");

	cout << "Pick a class: " << endl
		<< "\t [ 1 ] Warrior" << endl
		<< "\t [ 2 ] Assassin" << endl
		<< "\t [ 3 ] Mage" << endl
		<< endl;
	cin >> classChoice;

	while (classChoice > 3 || classChoice <= 0)	{
		cout << "INVALID INPUT! Please try again" << endl;
		cout << "Pick a class: " << endl
			<< "\t [ 1 ] Warrior" << endl
			<< "\t [ 2 ] Assassin" << endl
			<< "\t [ 3 ] Mage" << endl
			<< endl;
		cin >> classChoice;
	}
	system("cls");

	unit = new Unit(name, classChoice);

	return unit;
}

void displayStats(Unit* unit)
{
	cout << "==============================================================================================="<< endl
		<< "Name: "		<< unit->getName() << " \t \t \t HP: " << unit->getHp() << " / " << unit->getMaxHp() << endl
		<< "Class: "	<< unit->getClass() << endl
		<< "================================================================================================"<< endl
		<< "POW: "		<< unit->getPower()																	 << endl
		<< "VIT: "		<< unit->getVitality()																 << endl
		<< "AGI: "		<< unit->getAgility()																 << endl
		<< "DEX: "		<< unit->getDexterity()																 << endl
		<< endl;
}


void playRound(Unit*player, Unit* enemy)
{
	while (player->getHp() > 0 && enemy->getHp() > 0) {
		displayStats(player);
		displayStats(enemy);
		system("pause>nul");

		combat(player, enemy);
		system("cls");
	}

}

void combat(Unit* player, Unit* enemy)
{
	if (player->getFirstToAttack()) {
		player->attack(enemy);
		if (player->getHit() == ATTACK_MISS) cout << player->getName() << "'s Attack Missed! " << endl;
		else if (player->getHit() == ATTACK_HIT) cout << player->getName() << " dealt " << player->getDamage() << " Damage" << endl;
		system("pause>nul");
		if (enemy->getHp() == 0) return;

		enemy->attack(player);
		if (enemy->getHit() == ATTACK_MISS) cout << enemy->getName() << "'s Attack Missed! " << endl;
		else if (enemy->getHit() == ATTACK_HIT) cout << enemy->getName() << " dealt " << enemy->getDamage() << " Damage" << endl;
		system("pause>nul");
		return;
	}


	else if (enemy->getFirstToAttack()) {
		enemy->attack(player);
		if (enemy->getHit() == ATTACK_MISS) cout << enemy->getName() << "'s Attack Missed! " << endl;
		else if (enemy->getHit() == ATTACK_HIT) cout << enemy->getName() << " dealt " << enemy->getDamage() << " Damage" << endl;
		system("pause>nul");
		if (player->getHp() == 0) return;

		player->attack(enemy);
		if (player->getHit() == ATTACK_MISS) cout << player->getName() << "'s Attack Missed! " << endl;
		else if (player->getHit() == ATTACK_HIT) cout << player->getName() << " dealt " << player->getDamage() << " Damage" << endl;
		system("pause>nul");
		return;
	}
}

