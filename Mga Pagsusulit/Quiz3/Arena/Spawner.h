
#pragma once

#include "Unit.h"

class Spawner
{
public:
	Spawner();
	~Spawner();
	Unit* spawn();
	int getNumberOfSpawns();

private:
	Unit* mUnit;
	int mRandomClass;
	int mNumberOfSpawns;
};

