#pragma once

#include <string>
#include <iostream>

using namespace std;

// Class Types
const string CLASS_WARRIOR	= "Warrior"	;
const string CLASS_ASSASSIN = "Assassin";
const string CLASS_MAGE		= "Mage"	;

// Class Choices
const int CHOICE_WARRIOR	= 1;
const int CHOICE_ASSASSIN	= 2;
const int CHOICE_MAGE		= 3;

// Multipliers
const double MULTIPLIER_DAMAGE	= 0.50f;
const double MULTIPLER_HEAL		= 0.30f;

// Advantage
const bool ADVANTAGE = true;

// Win Bonus
const int BONUS_THREE	= 3;
const int BONUS_FIVE	= 5;

// Minimum and maximum values
const int HIT_RATE_MAX = 80;
const int HIT_RATE_MIN = 20;
const int DAMAGE_MIN = 1;

// Hit or miss
const bool ATTACK_HIT = true;
const bool ATTACK_MISS = false;

class Unit
{
public:
	// Constructors
	Unit(string name, int classChoice);
	Unit(int classChoice, int level);

	// Getters and setters
	string getName();
	string getClass();

	int getHp();
	int getMaxHp();
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();

	int getDamage();
	int getHitRate();
	int getHeal();
	int getBonusDamage();


	string getTargetClass();
	bool getFirstToAttack();
	bool getHit();

	void setClassStats(const int classChoice);

	// Primary Actions
	void attack(Unit* target);
	
	// Secondary Actions
	void check(const Unit* target);	// Inspects the enemy
	void lvlUp();					// Grants this Unit Bonus Stats on Victory


private:
	// Info
	string mName;
	string mClass;

	// Basic Stats
	int mHp;
	int mMaxHp;
	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;

	// Derived Stats
	int mDamage;
	int mHitrate;
	int	 mHeal;
	int	 mBonusDamage;

	// Miscellaneous 
	bool mAdvantage;	// If the player has the advantage
	string mTargetClass;// This Unit keeps in mind the class of this Unit's Opponent for reference
	int mChanceOfHit;	// The random chance if the attack will Hit/Miss
	bool mFirstToAttack;//if this unit will attack first
	bool mHit;			// if the attack is a hit or miss
};