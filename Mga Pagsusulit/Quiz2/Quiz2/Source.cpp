//=====================================================================================================================================================================================================
//============================                QUIZ # 2 : THE WALL                ======================================================================================================================
//=====================================================================================================================================================================================================

#include"Node.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

// Gets the number of soldiers that will play
int getInput();

// Choose how many soldiers will participate the game
Node* dynamicMembers(Node* node, int size);

// Print the soldiers names
void printRemainingSoldiers(Node* soldiers, int size);

// Selecting the starting point
void initialSelection(Node*& soldiers, int size);

// Member Elimination
void elimination(Node*& soldiers, int size);

// Plays 1 round of The Wall
void playRound(Node*& soldiers, int& rounds, int& numberOfSoldiers);

// Generating a random number and eliminating the soldier based on that random number
void mainGame(Node* soldiers);


int main()
{
	srand(time(NULL));

	Node* node = new Node;

	mainGame(node);

	system("pause>nul");
	return 0;
}


int getInput()
{	
	int size;
	const int MAX_SIZE = 50; // Maximum input value
	const int MIN_SIZE = 0; // minimum input value

	cout << "Enter number of soldiers(Maximum of 50 Soldiers): ";
	cin >> size;
	cout << '\n';
	system("cls");

	// Invalid Input
	bool isInvalidInput = size > MAX_SIZE || size <= MIN_SIZE;
	while (isInvalidInput) {
		cout << "Invalid number of players! Please try again: ";
		cin >> size;
		cout << '\n';
		system("cls");
	}

	return size;
}


Node* dynamicMembers(Node* node, int size)
{
	Node* head = node;

	// Allocating the nodes based on the User Input
	for (int i = 1; i < size;i++) {
		node->next = new Node;
		node = node->next;
	}

	// Completing the circular linked by connecting the last node to the head node
	node->next = head;

	// After creating the linked list, Inout the names of each node
	for (int i = 1; i <= size; i++) {
		cout << "Enter name of Soldier #" << i << ": ";
		cin >> node->name;
		node = node->next;
	}


	system("pause");
	system("cls");
	return node;
}


void printRemainingSoldiers(Node* soldiers, int size)
{
	cout << "The soldiers are: \n";

	for (int i = 0; i < size; i++) {
		cout << soldiers->name << '\n';
		soldiers = soldiers->next;
	}

	cout << '\n';
}


void initialSelection(Node*& soldiers, int size)
{
	soldiers = dynamicMembers(soldiers, size);

	// Selecting who will be the first one who will hold the cloak
	int selectedMember = rand() % 100;
	for (int i = 0; i < selectedMember; i++) {
		soldiers = soldiers->next;
	}

	cout << "The first one who will take the cloak is: " << soldiers->name << '\n';

	system("pause>nul");
	system("cls");
}


void elimination(Node*& soldiers, int size)
{
	//The player who holds the cloak draws a random number
	int eliminatedMember = rand() % size + 1;
	cout << soldiers->name << " has drawn " << eliminatedMember << '\n';

	// The previous node
	Node* prev = soldiers;

	// Passing of the cloak
	for (int i = 0; i < eliminatedMember; i++) {
		// The current node
		soldiers = soldiers->next;

		// The previous node (1 node before the current node)
		if (i < (eliminatedMember - 1))
			prev = prev->next;
	}

	system("pause>nul");

	cout << soldiers->name << " is eliminated \n";

	// Fixing the link by connecting the previous node to the next node
	prev->next = soldiers->next;

	// Deallocating the current node
	delete soldiers;

	// Assigning the current node to the next node
	soldiers = prev->next;
}


void playRound(Node*& soldiers, int& rounds, int& numberOfSoldiers)
{
	cout << "Round " << rounds << "\n \n";
	printRemainingSoldiers(soldiers, numberOfSoldiers);

	elimination(soldiers, numberOfSoldiers);

	rounds++;
	numberOfSoldiers--;

	system("pause>nul");
	system("cls");
}


void mainGame(Node* soldiers)
{
	int numberOfSoldiers = getInput();

	initialSelection(soldiers, numberOfSoldiers);

	// Plays until there is only one player left
	int rounds = 1;
	while (numberOfSoldiers != 1) {
		playRound(soldiers, rounds, numberOfSoldiers);
	}

	cout << soldiers->name << " will go to seek the reinforcements! \n";
}


