#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int wager(int mm);
void emperorDeck(vector<string>& kaiji, vector<string>& tonegawa);
void slaveDeck(vector<string>& kaiji, vector<string>& tonegawa);
void playRound(vector<string>&  kaiji, vector<string>& tonegawa, int& perica, int bet, int& mm);
void evaluateEnding(int mm, int perica, int round);

int main()
{
	srand(time(NULL));
	vector<string>  kaiji, tonegawa;
	int mm = 30;
	int perica = 0;
	int round = 1;

	while (round <= 12 && mm != 0) {
		cout << "\n Remaining Distance: " << mm << endl << " Perica: " << perica << endl;
		int bet = wager(mm);
		cout << "Round " << round << endl;
		emperorDeck(kaiji, tonegawa);
		playRound(kaiji, tonegawa, perica, bet, mm);
		round++;
	}
	evaluateEnding(mm, perica, round);
	system("pause");
	system("cls");

	return 0;
}

int wager(int mm)
{
	int bet;

	do {
		cout << "Enter your Bet:";
		cin >> bet;
		system("cls");
	} while (bet > mm || bet <= 0);

	return bet;
}
void emperorDeck(vector<string>& kaiji, vector<string>& tonegawa)
{
	kaiji.push_back("Emperor");
	tonegawa.push_back("Slave");

	for (int i = 0; i < 4; i++) {
		kaiji.push_back("Citizen");
		tonegawa.push_back("Citizen");
	}
}
void slaveDeck(vector<string>& kaiji, vector<string>& tonegawa)
{
	kaiji.push_back("Slave");
	tonegawa.push_back("Emperor");

	for (int i = 0; i < 4; i++) {
		kaiji.push_back("Citizen");
		tonegawa.push_back("Citizen");
	}
}

void playRound(vector<string>&  kaiji, vector<string>& tonegawa, int& perica, int bet, int& mm)
{
	int enemyChoice;
	int choice;
	do {
		enemyChoice = rand() % tonegawa.size();
		cout << "\n Remaining Distance: " << mm << endl << " Perica: " << perica << endl;
		cout << "\n CHOOSE A CARD: " << endl;

		for (int i = 0; i < kaiji.size(); i++)
			cout << "\t [ " << i + 1 << " ] " << kaiji[i] << endl;

		cin >> choice;

		if (choice == 1)
		{
			if (enemyChoice == 0) {
				cout << "You Chose " << kaiji[0] << endl << "Enemy Chose " << tonegawa[enemyChoice];
				mm -= bet;
			}
			else {
				cout << "You Chose " << kaiji[0] << endl << "Enemy Chose " << tonegawa[enemyChoice];
				perica += (bet * 100000);
			}
			kaiji.clear();
			tonegawa.clear();
			break;
		}
		else if (choice == 2 || choice == 3 || choice == 4 || choice == 5)
		{
			if (enemyChoice == 0) {
				cout << "You Chose " << kaiji[choice - 1] << endl << "Enemy Chose " << tonegawa[enemyChoice];
				perica += (bet * 100000);
				kaiji.clear();
				tonegawa.clear();
				break;
			}
			else {
				cout << "You Chose " << kaiji[choice - 1] << endl << "Enemy Chose " << tonegawa[enemyChoice];
				tonegawa.erase(tonegawa.begin() + enemyChoice);
				kaiji.erase(kaiji.begin() + choice - 1);
			}
			system("pause>nul");
		}

		system("cls");

	} while (choice != 1 || enemyChoice != 0);
	system("pause>nul");
	system("cls");

}


void evaluateEnding(int mm, int perica, int round)
{
	if (mm <= 0 && perica < 20000000) cout << "Bad Ending" << endl;
	if (round == 13 && mm > 0 && perica < 20000000) cout << "Meh Ending" << endl;
	if (round == 13 && mm > 0 && perica >= 20000000) cout << "Good Ending" << endl;
}