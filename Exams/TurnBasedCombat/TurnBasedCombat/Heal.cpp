#include "Heal.h"



Heal::Heal() : Actions("Heal", 10)
{
}


Heal::~Heal()
{
}

void Heal::use(Unit * actor, vector<Unit*> targets)
{
	Unit* target;
	vector<Unit*> validTargets;

	for (int i = 0; i < targets.size(); i++) {
		if (targets[i]->getTeam() == actor->getTeam())
		{
			validTargets.push_back(targets[i]);
		}
	}
	target = validTargets[rand() % validTargets.size()];
	int heal = target->getMaxHp() * 0.3;
	target->healHp(heal);


}
