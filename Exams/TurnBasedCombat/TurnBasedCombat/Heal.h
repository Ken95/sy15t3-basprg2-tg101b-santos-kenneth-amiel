#pragma once
#include "Actions.h"
class Heal :
	public Actions
{
public:
	Heal();
	~Heal();

	void use(Unit* actor, vector<Unit*> targets);

};

