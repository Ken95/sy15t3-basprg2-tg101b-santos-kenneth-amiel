#include "BasicAttack.h"



BasicAttack::BasicAttack() : Actions("Basic Attack", 0)
{

}


BasicAttack::~BasicAttack() 
{
}

void BasicAttack::use(Unit * actor, vector<Unit*> targets)
{
	Unit* target;
	vector<Unit*> validTargets;

	for (int i = 0; i < targets.size(); i++) {
		if (targets[i]->getTeam() != actor->getTeam())
		{
			validTargets.push_back(targets[i]);
		}
	}
	int maxdamage = actor->getPower() * 1.2;
	int mindamage = actor->getPower();

	int randpower = rand() % (maxdamage - mindamage) + mindamage;
	int basedamage = randpower * 1.0f;
	target = validTargets[rand() % validTargets.size()];
	int damage = (basedamage - target->getVitality());
	

	target->takeDamage(damage);
	actor->deductMp(getMpCost());

}
