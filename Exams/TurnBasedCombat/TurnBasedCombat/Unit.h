#pragma once
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iostream>
using namespace std;
#include "Actions.h"

class Actions;

class Unit
{
public:
	Unit(string name, string classType, string team);
	~Unit();

	void addAction(Actions* actions);
	string getName();
	string getClass();
	string getTeam();

	int getHp();
	int getMaxHp();
	int getMp();
	int getMaxMp();
	vector<Actions*> getAction();

	int getPower();
	int getVitality();
	int getDexterity();
	int getAgility();

	void takeDamage(int damage);
	void healHp(int heal);
	void deductMp(int mpCost);
	void useSkill(vector<Unit*> turnOrder, int choice);
private: 
	string mName;
	string mClassType;
	string mTeam;

	int mMaxHp;
	int mHp;
	int mMaxMp;
	int mMp;

	int mPower;
	int mVitality;
	int mDexterity;
	int mAgility;
	vector<Actions*> mActions;

};

