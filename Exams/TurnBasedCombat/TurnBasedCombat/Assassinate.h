#pragma once
#include "Actions.h"
class Assassinate :
	public Actions
{
public:
	Assassinate();
	~Assassinate();

	void use(Unit* actor, vector<Unit*> targets);
};

