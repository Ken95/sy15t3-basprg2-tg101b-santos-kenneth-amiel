#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iostream>
using namespace std;
#include "Unit.h"
#include "BasicAttack.h"
#include "Assassinate.h"
#include "Heal.h"
#include "Shockwave.h"


void printStatus(vector<Unit*> playerTeam, vector<Unit*> enemyTeam, vector<Unit*> turnOrder)
{
	cout << "=======" << playerTeam[0]->getTeam() << "========" << endl;
	for (int i = 0; i < playerTeam.size(); i++)
	{
		cout << playerTeam[i]->getName() << "\t HP: " << playerTeam[i]->getHp() << endl;
	}
	cout << endl;
	cout << "=======" <<enemyTeam[0]->getTeam() << "========" << endl;
	for (int i = 0; i < enemyTeam.size(); i++)
	{
		cout << enemyTeam[i]->getName() << "\t HP: " <<enemyTeam[i]->getHp() << endl;

	}
	cout << endl;
	cout << "=======" << "TURN ORDER" << "========" << endl;
	for (int i = 0; i < turnOrder.size(); i++)
	{
		cout << turnOrder[i]->getName() << endl;
	}
	system("pause");
	system("cls");
}
int main()
{
	Actions* basicAttack = new BasicAttack;
	Actions* shockwave = new Shockwave;
	Actions* assassinate = new Assassinate;
	Actions* heal = new Heal;

	Unit* p1 = new Unit("Lightning", "Warrior", "[Team] Harmony");
	p1->addAction(basicAttack);
	p1->addAction(shockwave);
	Unit* p2 = new Unit("Zidane  ", "Assassin", "[Team] Harmony");
	p2->addAction(basicAttack);
	p2->addAction(assassinate);
	Unit* p3 = new Unit("Tera      ", "Mage", "[Team] Harmony");
	p3->addAction(basicAttack);
	p3->addAction(heal);

	Unit* e1 = new Unit("Garland ", "Warrior", "[Team] Discord");
	e1->addAction(basicAttack);
	e1->addAction(shockwave);
	Unit* e2 = new Unit("Sephiroth", "Assassin", "[Team] Discord");
	e2->addAction(basicAttack);
	e2->addAction(assassinate);
	Unit* e3 = new Unit("Ultimecia", "Mage", "[Team] Discord");
	e3->addAction(basicAttack);
	e3->addAction(heal);

	vector<Unit*> playerTeam = { p1, p2, p3 };
	vector<Unit*> enemyTeam = { e1, e2, e3 };
	vector<Unit*> turnOrder = { p1, p2, p3, e1, e2, e3 };
	Unit* current = turnOrder[0];
	while (true)
	{
		int playerChoice;
		int enemyChoice = rand() % 2 + 1;
		current = turnOrder[0];
		printStatus(playerTeam, enemyTeam, turnOrder);  

		if (turnOrder[0]->getTeam() == "[Team] Harmony")
		{
			cout << "Choose an attack \n [1] Basic Attack < MP: 0 > \n [2] " << turnOrder[0]->getAction()[1]->getName()  << " < MP: 10 > " << endl; 
			cin >> playerChoice;

			while ((playerChoice != 1 && playerChoice != 2) || (playerChoice == 2 && turnOrder[0]->getAction()[1]->getMpCost() > turnOrder[0]->getMp()))
			{
				cout << "INVALID INPUT || NO MANA LEFT" << endl;
				cout << "Choose an attack \n [1] Basic Attack < MP: 0 > \n [2] " << turnOrder[0]->getAction()[1]->getName() << " < MP: 10 > " << endl;
				cin >> playerChoice;
			}
			turnOrder[0]->useSkill(turnOrder, playerChoice);
		}
		else if (turnOrder[0]->getTeam() == "[Team] Discord")
		{
			if (enemyChoice == 2 && turnOrder[0]->getAction()[1]->getMpCost() > turnOrder[0]->getMp()) enemyChoice = 1;
			turnOrder[0]->useSkill(turnOrder, enemyChoice);
		}


		turnOrder.erase(turnOrder.begin());
		turnOrder.push_back(current);
		system("pause");
		system("cls");
	}



	system("pause");
	return 0;
}