#pragma once
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iostream>
using namespace std;
#include "Unit.h"
class Unit;
class Actions
{
public:
	Actions(string name, int mpCost);
	~Actions();
	virtual void use(Unit* mActor, vector<Unit*> targets) = 0;
	string getName();
	int getMpCost();
	
private:
	string mName;
	int mMpCost;
};

