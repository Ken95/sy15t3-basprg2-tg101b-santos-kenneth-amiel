#include "Shockwave.h"



Shockwave::Shockwave() : Actions("Shock Wave", 10)
{
}


Shockwave::~Shockwave()
{
}

void Shockwave::use(Unit * actor, vector<Unit*> targets)
{
	vector<Unit*> validTargets;

	for (int i = 0; i < targets.size(); i++) {
		if (targets[i]->getTeam() != actor->getTeam())
		{
			validTargets.push_back(targets[i]);
		}
	}
	for (int i = 0; i < validTargets.size(); i++) {

		int maxdamage = actor->getPower() * 1.2;
		int mindamage = actor->getPower();
		int randpower = rand() % (maxdamage - mindamage) + mindamage;
		int basedamage = randpower * 0.9f;
		int damage = (basedamage - targets[i]->getVitality());
		validTargets[i]->takeDamage(damage);
	}

	actor->deductMp(getMpCost());
}
