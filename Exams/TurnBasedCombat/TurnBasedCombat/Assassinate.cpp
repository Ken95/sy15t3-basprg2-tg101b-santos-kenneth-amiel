#include "Assassinate.h"



Assassinate::Assassinate() : Actions("Assassinate", 10)
{
}


Assassinate::~Assassinate()
{
}

void Assassinate::use(Unit * actor, vector<Unit*> targets)
{
	Unit* target;
	vector<Unit*> validTargets;

	for (int i = 0; i < targets.size(); i++) {
		if (targets[i]->getTeam() != actor->getTeam())
		{
			validTargets.push_back(targets[i]);
		}
	}
	target = validTargets[rand() % validTargets.size()];
	int maxdamage = actor->getPower() * 1.2;
	int mindamage = actor->getPower();
	int randpower = rand() % (maxdamage - mindamage) + mindamage;
	int basedamage = randpower * 2.2f;
	int damage = (basedamage - target->getVitality());


	target->takeDamage(damage);
	actor->deductMp(getMpCost());

}
