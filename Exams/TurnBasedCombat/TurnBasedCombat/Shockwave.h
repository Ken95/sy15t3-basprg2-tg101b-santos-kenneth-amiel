#pragma once
#include "Actions.h"

class Shockwave :
	public Actions
{
public:
	Shockwave();
	~Shockwave();

	void use(Unit* actor, vector<Unit*> targets);
};

