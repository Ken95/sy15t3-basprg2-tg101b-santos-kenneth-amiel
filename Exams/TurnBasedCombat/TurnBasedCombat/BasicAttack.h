#pragma once
#include "Actions.h"
class BasicAttack :
	public Actions
{
public:
	BasicAttack();
	~BasicAttack();

	void use(Unit* actor, vector<Unit*> targets);
};

