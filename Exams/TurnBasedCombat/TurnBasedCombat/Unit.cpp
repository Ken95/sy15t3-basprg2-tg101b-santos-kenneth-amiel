#include "Unit.h"



Unit::Unit(string name, string classType, string team)
{
	mName = name;
	mClassType = classType;
	mMaxHp = rand() % 100 + 100;
	mHp = mMaxHp;
	mMaxMp = rand() % 50 + 50;
	mMp = mMaxMp;

	mPower = rand() % 50 + 50;
	mVitality = rand() % 50 + 1;
	mDexterity = rand() % 50 + 50;
	mAgility = rand() % 50 + 1;
	mTeam = team;
}


Unit::~Unit()
{
}

void Unit::addAction(Actions * actions)
{
	mActions.push_back(actions);
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mClassType;
}

string Unit::getTeam()
{
	return mTeam;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getMp()
{
	return mMp;
}

int Unit::getMaxMp()
{
	return mMaxMp;
}

vector<Actions*> Unit::getAction()
{
	return mActions;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getDexterity()
{
	return mDexterity;
}

int Unit::getAgility()
{
	return mAgility;
}

void Unit::takeDamage(int damage)
{
	if (damage < 1) damage = 1;

	cout << mName << " recieved " << damage << " damage" << endl;
	mHp -= damage;

}

void Unit::healHp(int heal)
{
	if (heal < 1) heal = 1;

	cout << mName << " recieved " << heal << " hp" << endl;
	mHp += heal;
}

void Unit::deductMp(int mpCost)
{
	mMp -= mpCost;
}

void Unit::useSkill(vector<Unit*> turnOrder, int choice)
{
	mActions[choice - 1]->use(this, turnOrder);
}
